# StAcumen Overflow

## Get Started

1. Clone this repo
2. `npm install`
3. `npm start`

Install the Heroku CLI for your platform and brush up on [Angular 2](https://angular.io/docs/ts/latest/quickstart.html) and the [Angular Style Guide](https://angular.io/docs/ts/latest/guide/style-guide.html).

### (Optional) Sublime Config
	- With Package Control, install `Typescript`
	- Exclude `.js` files and the _node_modules_ folder