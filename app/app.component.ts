import { Component } from '@angular/core';

@Component({
    selector: 'so-app',
    templateUrl: 'app/app.component.html'
})

export class AppComponent {
  pageTitle: string = 'Stacumen Overflow';
  // subtitle: string = 'The one stop shop for all of your cloud development coding, config, and best practice questions.';
}
