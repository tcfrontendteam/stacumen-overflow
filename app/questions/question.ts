/* Defines the question entity */
export interface QuestionInterface {
	questionId: number;
	title: string;
	submitted: string;
	content: string;
	votes?: number;
	views?: number;
	rating?: number;
	submitter: string;
	[metaTags: string]: any;
}