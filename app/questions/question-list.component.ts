import { Component, OnInit }  from '@angular/core';
import { Router } from '@angular/router';

import { QuestionInterface } from './question';
import { QuestionService } from './question.service';

@Component({
    templateUrl: 'app/questions/question-list.component.html',
    styleUrls: ['app/questions/question-list.component.css']
})

export class QuestionListComponent implements OnInit {
    pageTitle: string = 'Question List';
    errorMessage: string;

    questions: QuestionInterface[];

    constructor(
        private _questionService: QuestionService,
        private router: Router) {}

    ngOnInit(): void {
        this._questionService.getQuestions()
                .subscribe(questions => this.questions = questions,
                           error => this.errorMessage = <any>error);
    }
}
