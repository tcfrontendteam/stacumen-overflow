"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var question_service_1 = require("./question.service");
var QuestionListComponent = (function () {
    function QuestionListComponent(_questionService, router) {
        this._questionService = _questionService;
        this.router = router;
        this.pageTitle = 'Question List';
    }
    QuestionListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._questionService.getQuestions()
            .subscribe(function (questions) { return _this.questions = questions; }, function (error) { return _this.errorMessage = error; });
    };
    return QuestionListComponent;
}());
QuestionListComponent = __decorate([
    core_1.Component({
        templateUrl: 'app/questions/question-list.component.html',
        styleUrls: ['app/questions/question-list.component.css']
    }),
    __metadata("design:paramtypes", [question_service_1.QuestionService,
        router_1.Router])
], QuestionListComponent);
exports.QuestionListComponent = QuestionListComponent;
//# sourceMappingURL=question-list.component.js.map