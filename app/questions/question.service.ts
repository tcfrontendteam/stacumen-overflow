import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { QuestionInterface } from './question';

@Injectable()
export class QuestionService {
    private _questionUrl = 'api/questions/questions.json';

    constructor(private _http: Http) { }

    getQuestions(): Observable<QuestionInterface[]> {
        return this._http.get(this._questionUrl)
            .map((response: Response) => <QuestionInterface[]> response.json())
            //.do(data => console.log('All: ' +  JSON.stringify(data)))
            .catch(this.handleError);
    }

    getQuestion(id: number): Observable<QuestionInterface> {
        return this.getQuestions()
            .map((questions: QuestionInterface[]) => questions.find(p => p.questionId === id));
    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}