import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent }  from './app.component';
import { WelcomeComponent } from './home/welcome.component';
import { QuestionListComponent }  from './questions/question-list.component';
import { QuestionService } from './questions/question.service';
import { QuestionDetail } from './question-detail/question-detail.component';
import { AppRoutingModule } from './app-routing.module';

@NgModule({
  imports: [ 
  	BrowserModule,
	  HttpModule,
    AppRoutingModule
  ],
  declarations: [
      AppComponent,
      WelcomeComponent,
      QuestionListComponent,
      QuestionDetail
  ],
  providers: [QuestionService],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
