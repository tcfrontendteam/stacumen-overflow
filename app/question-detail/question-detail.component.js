"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var question_service_1 = require("../questions/question.service");
var QuestionDetail = (function () {
    function QuestionDetail(questionService, route, router) {
        this.questionService = questionService;
        this.route = route;
        this.router = router;
    }
    QuestionDetail.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.forEach(function (params) {
            var questionId = parseInt(params['id']);
            _this.questionService.getQuestion(questionId)
                .subscribe(function (question) {
                if (question) {
                    _this.question = question;
                }
                else {
                    // No question found, re-routing back to the questions list page
                    _this.router.navigate(['/questions']);
                }
            }, function (error) { return console.error("Error: " + _this); });
        });
    };
    return QuestionDetail;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], QuestionDetail.prototype, "question", void 0);
QuestionDetail = __decorate([
    core_1.Component({
        templateUrl: 'app/question-detail/question-detail.component.html',
        styleUrls: ['app/question-detail/question-detail.component.css']
    }),
    __metadata("design:paramtypes", [question_service_1.QuestionService,
        router_1.ActivatedRoute,
        router_1.Router])
], QuestionDetail);
exports.QuestionDetail = QuestionDetail;
//# sourceMappingURL=question-detail.component.js.map