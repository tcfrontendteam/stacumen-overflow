import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params }   from '@angular/router';

import { QuestionService } from '../questions/question.service';
import { QuestionInterface } from '../questions/question';

@Component({
  templateUrl: 'app/question-detail/question-detail.component.html',
  styleUrls: ['app/question-detail/question-detail.component.css']
})

export class QuestionDetail implements OnInit {
  @Input() question: QuestionInterface;
  
  constructor(
    private questionService: QuestionService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.params.forEach((params: Params) => {
      let questionId = parseInt(params['id']);
      
      this.questionService.getQuestion(questionId)
        .subscribe(question => {
          if (question) {
            this.question = question;
          } else {
            // No question found, re-routing back to the questions list page
            this.router.navigate(['/questions']);
          }
        }, 
        error => console.error("Error: " + this));
    });
  }
}
