import { Component } from '@angular/core';

@Component({
    templateUrl: 'app/home/welcome.component.html'
})
export class WelcomeComponent {
    public pageTitle: string = 'Welcome to StAcumen Overflow';
    public tagLine: string = 'Your one stop shop for Salesforce and web development questions';
    public signUp: string = 'Sign Up';
}
