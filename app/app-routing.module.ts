import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { WelcomeComponent } from './home/welcome.component';
import { QuestionListComponent } from './questions/question-list.component';
import { QuestionDetail } from './question-detail/question-detail.component';

const routes: Routes = [
  { path: 'home',  component: WelcomeComponent },
  { path: 'questions', component: QuestionListComponent },
  { path: 'questions/:id', component: QuestionDetail },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}
