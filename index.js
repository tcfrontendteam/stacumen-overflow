'use strict';

const express = require('express');

var app = express();

// set the static files location /public/img will be /img for users
app.use(express.static(__dirname + '/public'));

// Generic error handling:
app.use(function(err, req, res, next) {
	// prints stack trace to the console
	console.error(err.stack);

	// Sends generic error page to the user
  	res.status(500).send('Error!');
});

const port = process.env.PORT || 8080;
app.listen(port, function() {
  console.log('Node app is running on port', port);
});

app.get('/', function(req, res) {
	res.status(200).send('Let\'s get routey');
});